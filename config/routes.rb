Rails.application.routes.draw do
  get 'sessions/new'

  get 'users/new'

  root 'startup#index'
 # get 'startup/index'
  
  get    '/help',     to: 'startup#help'
  get    '/about',    to: 'startup#about'
  #get    '/upload',   to: 'upload#fileUpload'
  #post   '/uploadFile',   to: 'upload#uploadFile'
  get    '/register', to: 'users#new'
  get    '/signup',   to: 'users#new'
  get    '/login',    to: 'sessions#new'
  post   '/login',    to: 'sessions#create'
  delete '/logout',   to: 'sessions#destroy'
  post '/downloadFile', to: 'users#downloadFile'
  #get  '/downloadFile', to: 'users#downloadFileLink'
  #post '/uploadFile', to: 'users#uploadFile'
  resources :users
end
