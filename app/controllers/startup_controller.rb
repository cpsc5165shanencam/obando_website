class StartupController < ApplicationController
  
  def index
  end
  
  def about
  end

  def help
  end
  
  def upload
  end

  def login
  end
  
  def register
  end
end
