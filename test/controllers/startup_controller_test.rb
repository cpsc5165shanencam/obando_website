require 'test_helper'

class StartupControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "CSU Games Host"
  end
  
  test "should get root" do
    get root_url
    assert_response :success
  end
  
  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "Home | #{@base_title}"
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "About | #{@base_title}"
  end
  
  test "should get help" do
    get help_path
    assert_response :success
    assert_select "title", "Help | #{@base_title}"
  end
  
  test "should get upload" do
    get upload_path
    assert_response :success
    assert_select "title", "Upload | #{@base_title}"
  end
  
  test "should get login" do
    get login_path
    assert_response :success
    assert_select "title", "Login | #{@base_title}"
  end
  
  test "should get register" do
    get signup_path
    assert_response :success
    assert_select "title", "Sign up | #{@base_title}"
  end
end
